import numpy as np
from matplotlib import pyplot as plt

a_priori = np.linspace(0.00,0.10,100)

positive_if_ill = 0.97
positive_if_not_ill = 0.05

def bayes_formula(positive_if_ill,positive_if_not_ill,a_priori):
    num = positive_if_ill * a_priori
    denom_1 = positive_if_ill * a_priori
    denom_2 = positive_if_not_ill * (1-a_priori)
    res = num / (denom_1+denom_2)
    return res

res_one_time = bayes_formula(positive_if_ill,positive_if_not_ill,a_priori)
res_two_times = bayes_formula(positive_if_ill**2,positive_if_not_ill**2,a_priori)
res_three_times = bayes_formula(positive_if_ill**3,positive_if_not_ill**3,a_priori)

plt.figure()
plt.title("Are you ill?")
plt.plot(a_priori*100,res_one_time*100,"r--",label = "at one time tested positive")
plt.plot(a_priori*100,res_two_times*100,"b--",label = "at two times tested positive")
plt.plot(a_priori*100,res_three_times*100,"g--",label = "at three times tested positive")
plt.ylabel("P(ill | test is potitive) [%]")
plt.xlabel("a priori probability (that you are ill) [%]")
plt.legend()
plt.grid()
plt.show()
